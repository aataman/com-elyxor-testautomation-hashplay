Pull Docker Image
=================
* assuming docker user and repo: robjahn/sandbox

1. docker signin
2. docker pull robjahn/sandbox

To Run Docker as a container
============================
docker run -d -p 4200:4200 --name ngdemo robjahn/sandbox

Access App
----------
http://localhost:4200

Remove container when done
--------------------------
docker rm -f ngdemo

Build Docker Image
==================
* assuming docker user and repo: robjahn/sandbox

1. install docker.  Must download from the docker website
2. create project directory e.g. ~/dev/angular-tour-of-heroes
3. unzip toh-pt6.zip.  Zip taken from https://angular.io/tutorial to project home
4. docker build -t robjahn/sandbox .
5. docker images
6. docker signin
7. docker push robjahn/sandbox
8. verify in https://hub.docker.com/r/robjahn/sandbox/tags/
