### Setup

Requires Chrome browser installed on host running test suite. Path to Chrome defined in:
```
src/test/resources/testautomation.properties
```

### Running Tests

to run suite 
```
$ gradle clean test
```

Tests to run are defined in this file in project root
```
testng.xml
```
