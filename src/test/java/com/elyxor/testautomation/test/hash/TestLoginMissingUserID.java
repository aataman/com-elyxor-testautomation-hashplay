package com.elyxor.testautomation.test.hash;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.pages.hash.HashHomePage;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestLoginMissingUserID extends TestBase {

    private static final Logger logger = LoggerFactory.getLogger(TestLoginMissingUserID.class);

    HashHomePage hashHomePage;

    @Test(enabled=true)
    public void loginMissingUserID() {

        webDriver.get(baseurl);
        hashHomePage = PageFactory.initElements(webDriver, HashHomePage.class);

        // TODO read from input service once ready not config service
        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        String password = String.format("%s", configInstance.getValue("test.loginmissinguserid.password"));
        String expectedErrorText = String.format("%s", configInstance.getValue("test.loginmissinguserid.errortext"));

        hashHomePage.login("", password);

        String actualErrorText = hashHomePage.getErrorText();
        Assert.assertEquals(actualErrorText,expectedErrorText);

        hashHomePage.screenshot("loginmissinguserid.LoginPage");

    }
}
