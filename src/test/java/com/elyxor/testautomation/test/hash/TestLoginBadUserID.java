package com.elyxor.testautomation.test.hash;

import com.elyxor.testautomation.pages.hash.HashHomePage;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.testng.Assert;


public class TestLoginBadUserID extends TestBase {

    private static final Logger logger = LoggerFactory.getLogger(TestLoginBadUserID.class);

    HashHomePage hashHomePage;

    @Test(enabled=true)
    public void loginBadUserID() {

        webDriver.get(baseurl);
        hashHomePage = PageFactory.initElements(webDriver, HashHomePage.class);

        // this shows use of the TestData utils to print out what found
        testData.printTestCaseData("TestLoginBadUserID");
        testData.printTestCaseData("InvalidTestCase");

        // get the TestData values
        String user = testData.get("TestLoginBadUserID", "user");
        String password = testData.get("TestLoginBadUserID", "password");
        String expectedErrorText = testData.get("TestLoginBadUserID", "errortext");

        hashHomePage.setUser(user);
        hashHomePage.setPassword(password);

        // this shows use of the custom Element
        hashHomePage.customElement.click();
        this.pauseTest(3000);

        // do the assertion
        String actualErrorText = hashHomePage.getErrorText();
        Assert.assertEquals(expectedErrorText,actualErrorText);

    }
}


// NOT USED ANYMORE
//TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
//String user = String.format("%s", configInstance.getValue("test.loginbaduserid.user"));
//String password = String.format("%s", configInstance.getValue("test.loginbaduserid.password"));
//String expectedErrorText = String.format("%s", configInstance.getValue("test.loginbaduserid.errortext"));
