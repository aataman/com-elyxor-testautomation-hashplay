package com.elyxor.testautomation.test.hash;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.pages.hash.HashFlightPage;
import com.elyxor.testautomation.pages.hash.HashHomePage;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;


public class TestLoginSuccess extends TestBase {

    private static final Logger logger = LoggerFactory.getLogger(TestLoginSuccess.class);

    HashHomePage hashHomePage;
    HashFlightPage hashFlightPage;

    @Test(enabled=true)
    public void loginSuccess() {

        webDriver.get(baseurl);
        hashHomePage = PageFactory.initElements(webDriver, HashHomePage.class);

        // TODO read from input service once ready not config service
        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        String user = String.format("%s", configInstance.getValue("test.loginsuccess.user"));
        String password = String.format("%s", configInstance.getValue("test.loginsuccess.password"));

        hashHomePage.screenshot("loginsuccess.LoginPage");
        hashHomePage.login(user, password);

        hashFlightPage = PageFactory.initElements(webDriver, HashFlightPage.class);
        hashFlightPage.screenshot("loginsuccess.FlightsPage");


        //TODO: this objects not working, so add these test steps once they do
        //hashFlightPage.clickMenu();
        //hashFlightPage.clickMenuOptionLogout();

    }
}
