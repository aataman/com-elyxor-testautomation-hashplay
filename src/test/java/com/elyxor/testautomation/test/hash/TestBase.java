package com.elyxor.testautomation.test.hash;

import com.elyxor.testautomation.utils.driver.DriverFactory;
import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.utils.testdata.TestData;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


public class TestBase {

    protected WebDriver webDriver;
    protected TestData testData;

    protected String baseurl;
    protected String browser;

    private static final Logger logger = LoggerFactory.getLogger(TestLoginBadUserID.class);

    @BeforeClass
    public void beforeTest() throws Exception {

        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        baseurl = String.format("%s", configInstance.getValue("app.hash.baseurl"));
        browser = configInstance.getValue("browser.name");
        String testdatafile = configInstance.getValue("testdatafile");

        logger.info("Base URL: " + baseurl);
        logger.info("Browser: " + browser);

        webDriver = DriverFactory.getWebDriver(browser);

        // get test data
        testData = new TestData();
        testData.load(testdatafile);
    }

    @AfterClass
    public void afterTest() {
        if (webDriver != null) {
            webDriver.close();
            webDriver.quit();
        }
    }

    // used to just let the user actually see something!
    public void pauseTest(Integer waitTimeMS) {
        try {
            Thread.sleep(waitTimeMS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
