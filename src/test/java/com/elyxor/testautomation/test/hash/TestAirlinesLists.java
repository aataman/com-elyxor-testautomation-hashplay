package com.elyxor.testautomation.test.hash;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.pages.hash.HashAirlinesPage;
import com.elyxor.testautomation.pages.hash.HashFlightPage;
import com.elyxor.testautomation.pages.hash.HashHomePage;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestAirlinesLists extends TestBase {

    HashHomePage hashHomePage;
    HashAirlinesPage hashAirlinesPage;
    HashFlightPage hashFlightPage;


    private void succesfulLogin()
    {
        webDriver.get(baseurl);
        hashHomePage = PageFactory.initElements(webDriver, HashHomePage.class);


        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        String user = String.format("%s", configInstance.getValue("test.loginsuccess.user"));
        String password = String.format("%s", configInstance.getValue("test.loginsuccess.password"));
        hashHomePage.login(user, password);
    }



    @Test(enabled = true)
    public void AddNewAirlineConfirmAirlineName() {

        String airlineName = "My Airline";
        succesfulLogin();
        hashAirlinesPage = PageFactory.initElements(webDriver, HashAirlinesPage.class);
        hashAirlinesPage.AddNewItem(airlineName);
        hashAirlinesPage.sleep(2);


        Assert.assertTrue(hashAirlinesPage.checkAirlineExists(airlineName));

        //Cleanup
        hashAirlinesPage.sleep(2);
        hashAirlinesPage.RemoveItem(airlineName);
    }

    @Test(enabled=true)
    public void AddNewAirlineConfirmTotalAirlineNumber() {

        succesfulLogin();
        hashAirlinesPage = PageFactory.initElements(webDriver, HashAirlinesPage.class);

        String originalValue = hashAirlinesPage.GetNumberOfAirlines();
        int newValue = Integer.parseInt(originalValue) + 1;
        hashAirlinesPage.AddNewItem("My Airline");
        hashAirlinesPage.sleep(2);

        Assert.assertEquals(newValue, (Integer.parseInt(originalValue) + 1));

        //Cleanup
        hashAirlinesPage.RemoveItem("My Airline");
    }


    @Test(enabled = true)
    public void ClickOnEditButtonByAirlineNameThenUpdateConfirmAirlineName() {

        succesfulLogin();
        hashAirlinesPage = PageFactory.initElements(webDriver, HashAirlinesPage.class);

        hashAirlinesPage.AddNewItem("My Airline");
        hashAirlinesPage.sleep(2);
        hashAirlinesPage.clickEditButton("My Airline");
        hashAirlinesPage.sleep(2);
        hashAirlinesPage.updateAirlineName("_updated");
        hashAirlinesPage.sleep(2);
        hashAirlinesPage.clickUpdateButton();
        hashAirlinesPage.sleep(2);


        Assert.assertTrue(hashAirlinesPage.checkAirlineExists("My Airline_updated"));

        //Cleanup
        hashAirlinesPage.RemoveItem("My Airline_updated");
    }


    @Test(enabled = true)
    public void ClickOnDeleteButtonByRowNumberThenCancelConfirmUnchangedNumberOfAirlines() {

        succesfulLogin();
        hashAirlinesPage = PageFactory.initElements(webDriver, HashAirlinesPage.class);


        String originalValue = hashAirlinesPage.GetNumberOfAirlines();
        hashAirlinesPage.clickDeleteButton(1);
        hashAirlinesPage.sleep(2);
        hashAirlinesPage.clickCancel();
        String newValue = hashAirlinesPage.GetNumberOfAirlines();

        Assert.assertEquals(newValue, originalValue);

    }

    @Test(enabled = true)
    public void ClickOnDeleteButtonByAirlineNameThenCancelConfirmAirlineExists() {

        String airlineName = "My Airline";
        succesfulLogin();
        hashAirlinesPage = PageFactory.initElements(webDriver, HashAirlinesPage.class);
        hashAirlinesPage.AddNewItem(airlineName);
        hashAirlinesPage.sleep(2);


        hashAirlinesPage.clickDeleteButton(airlineName);
        hashAirlinesPage.sleep(2);
        hashAirlinesPage.clickCancel();


        Assert.assertTrue(hashAirlinesPage.checkAirlineExists(airlineName));

        //Cleanup
        hashAirlinesPage.sleep(2);
        hashAirlinesPage.RemoveItem(airlineName);
    }

}
