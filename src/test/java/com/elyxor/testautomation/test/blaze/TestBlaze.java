package com.elyxor.testautomation.test.blaze;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.*;


public class TestBlaze {
    ChromeDriver wd;

    @BeforeTest
    public void setUp() throws Exception {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBinary("/Applications/Google Chrome.app/Contents/MacOS/Google Chrome");

        System.setProperty("webdriver.chrome.driver", "/Users/robjahn/dev/com-elyxor-testautomation-sandbox/src/test/resources/chromedriver");
        //System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");

        wd = new ChromeDriver(chromeOptions);
        wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @Test
    public void blazeOpen() {
        wd.get("http://www.blazedemo.com/");
        if (!wd.findElement(By.xpath("//div[3]/form/select[1]//option[3]")).isSelected()) {
            wd.findElement(By.xpath("//div[3]/form/select[1]//option[3]")).click();
        }
        if (!wd.findElement(By.xpath("//div[3]/form/select[2]//option[6]")).isSelected()) {
            wd.findElement(By.xpath("//div[3]/form/select[2]//option[6]")).click();
        }
        wd.findElement(By.cssSelector("input.btn.btn-primary")).click();
        wd.findElement(By.xpath("//table[@class='table']/tbody/tr[4]/td[1]/input")).click();
        wd.findElement(By.cssSelector("input.btn.btn-primary")).click();
    }

    @AfterTest
    public void tearDown() {
        wd.quit();
    }

}