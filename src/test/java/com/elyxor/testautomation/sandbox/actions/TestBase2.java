package com.elyxor.testautomation.sandbox.actions;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.test.hash.TestLoginBadUserID;
import com.elyxor.testautomation.utils.driver.Driver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


public class TestBase2 {

    private static final Logger logger = LoggerFactory.getLogger(TestLoginBadUserID.class);

    @BeforeClass
    public void beforeTest() throws Exception {

        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        String browser = configInstance.getValue("browser.name");

        Driver driver = new Driver();
        driver.initializeDriver(browser);

    }

    @AfterClass
    public void afterTest() {

        Driver driver = new Driver();
        driver.closeDriver();
    }

    // used to just let the user actually see something!
    public void pauseTest(Integer waitTimeMS) {
        try {
            Thread.sleep(waitTimeMS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
