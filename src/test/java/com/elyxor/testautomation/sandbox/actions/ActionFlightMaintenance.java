package com.elyxor.testautomation.sandbox.actions;

import com.elyxor.testautomation.utils.driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ActionFlightMaintenance extends ActionBase {

    private static final String addAirlineLocator = "body > reference > app-root > app-dashboard > app-airlines > div > div > div > span > button > span";
    private static final String closeAirLinePopupLocator = "body > reference > app-root > app-dashboard > app-airlines > div > div > app-edit-airline-modal > div > div > div > div.modal-header > button";

    public ActionFlightMaintenance() throws Exception {
        super();
    }

    public void selectAddAirline() {
        WebDriver webDriver = Driver.webDriver;
        webDriver.findElement(By.cssSelector(addAirlineLocator)).click();
    }

    public void closeAddAirlinePopop() {
        WebDriver webDriver = Driver.webDriver;
        webDriver.findElement(By.cssSelector(closeAirLinePopupLocator)).click();
    }
}
