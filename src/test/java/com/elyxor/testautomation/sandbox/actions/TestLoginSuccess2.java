package com.elyxor.testautomation.sandbox.actions;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;


public class TestLoginSuccess2 extends TestBase2 {

    private static final Logger logger = LoggerFactory.getLogger(TestLoginSuccess2.class);

    @Test
    public void loginSuccess() throws Exception {

        ActionLogin actionLogin = new ActionLogin();
        ActionMenu actionMenu = new ActionMenu();
        ActionFlightMaintenance actionFlightMaintenance = new ActionFlightMaintenance();

        // TODO read from input service once ready not config service
        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        String url = String.format("%s", configInstance.getValue("app.hash.baseurl"));
        String user = String.format("%s", configInstance.getValue("test.loginsuccess.user"));
        String password = String.format("%s", configInstance.getValue("test.loginsuccess.password"));

        actionLogin.openApp(url);
        actionLogin.login(user,password);

        actionFlightMaintenance.selectAddAirline();
        this.pauseTest(2000);

        actionFlightMaintenance.closeAddAirlinePopop();
        this.pauseTest(2000);

        actionMenu.selectMenuItem("logout");

    }
}
