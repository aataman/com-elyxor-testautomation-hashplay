package com.elyxor.testautomation.sandbox.actions;

import com.elyxor.testautomation.utils.driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActionLogin extends ActionBase {

    private static final Logger logger = LoggerFactory.getLogger(ActionLogin.class);

    private static final String textBoxUserLocator = "body > reference > app-root > app-sign-in > div > div > div > div > div.card-block > div:nth-child(1) > input";
    private static final String textBoxPasswordLocator = "body > reference > app-root > app-sign-in > div > div > div > div > div.card-block > div:nth-child(2) > input";
    private static final String buttonLoginLocator = "/html/body/reference/app-root/app-sign-in/div/div/div/div/div[2]/button";
    private static final String errorTextLocator = "body > reference > app-root > app-sign-in > div > div > div > div > div.card-block > div.alert.alert-danger > span";

    public ActionLogin() throws Exception {
        super();
    }

    public void openApp(String url) {
        Driver.webDriver.get(url);
    }

    private WebElement getTextBoxUser() {
        WebDriver webDriver = Driver.webDriver;
        return webDriver.findElement(By.cssSelector(textBoxUserLocator));
    }

    private WebElement getTextBoxPassword() {
        WebDriver webDriver = Driver.webDriver;
        return webDriver.findElement(By.cssSelector(textBoxPasswordLocator));
    }

    private WebElement getButtonLogin() {
        WebDriver webDriver = Driver.webDriver;
        return webDriver.findElement(By.xpath(buttonLoginLocator));
    }

    public void login(String user, String password) {
        getTextBoxUser().sendKeys(user);
        getTextBoxPassword().sendKeys(password);
        getButtonLogin().click();
    }

    public String getLoginErrorText() {
        try {
            waitForObject(By.cssSelector(errorTextLocator));
            WebDriver webDriver = Driver.webDriver;
            WebElement errorText = webDriver.findElement(By.cssSelector(errorTextLocator));
            return errorText.getText();
        }
        catch (Exception e) {
            // TODO consider throw org.openqa.selenium.TimeoutException
            return "";
        }
    }
}
