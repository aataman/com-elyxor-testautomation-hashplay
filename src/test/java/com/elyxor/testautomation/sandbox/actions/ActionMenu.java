package com.elyxor.testautomation.sandbox.actions;

import com.elyxor.testautomation.utils.driver.Driver;
import org.apache.commons.lang3.exception.ContextedRuntimeException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ActionMenu extends ActionBase {
    //private static final String menuLocator = "body > reference > app-root > app-dashboard > app-airlines > div > div > div > span > button > span";
    private static final String menuLocator = "navbarDropdownMenuLink";
    private static final String menuItemLogoutLocator = "body > reference > app-root > app-dashboard > app-navbar > nav > ul:nth-child(2) > li > div > a > i";
    public ActionMenu() throws Exception {
        super();
    }

    private WebElement getMenu() {
        WebDriver webDriver = Driver.webDriver;
        waitForObject(By.id(menuLocator));
        return webDriver.findElement(By.id(menuLocator));
    }

    private WebElement getMenuItemLogout() {
        WebDriver webDriver = Driver.webDriver;
        return webDriver.findElement(By.cssSelector(menuItemLogoutLocator));
    }

    // TODO this is broken
    public void selectMenuItem(String menuitem) {
        switch (menuitem) {
            case "logout":
                getMenu().click();
                //getMenuItemLogout().click();
                break;

            default:
                throw new ContextedRuntimeException(String.format("Invalid menu item: %s", menuitem));

        }
    }
}
