package com.elyxor.testautomation.sandbox.actions;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.utils.datastore.DataStoreFactory;
import com.elyxor.testautomation.utils.datastore.model.TestDataStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestLoginBadUserID2 extends TestBase2 {

    private static final Logger logger = LoggerFactory.getLogger(TestLoginBadUserID2.class);

    @Test
    public void loginSuccess() throws Exception {

        ActionLogin actionLogin = new ActionLogin();

        // TODO read from input service once ready not config service
        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        String url = String.format("%s", configInstance.getValue("app.hash.baseurl"));
        String user = String.format("%s", configInstance.getValue("test.loginbaduserid.user"));
        String password = String.format("%s", configInstance.getValue("test.loginbaduserid.password"));
        String expectedErrorText = String.format("%s", configInstance.getValue("test.loginbaduserid.errortext"));

        actionLogin.openApp(url);
        actionLogin.login(user,password);

        String actualErrorText = actionLogin.getLoginErrorText();
        Assert.assertEquals(expectedErrorText,actualErrorText);

        logger.info("**************************************************************");
        TestDataStore tds = DataStoreFactory.getDataStore("testdata");
        tds.add("key1","data_original");
        logger.info("key1: " + tds.get("key1"));
        tds.clear();
        logger.info("key1: " + tds.get("key1"));
        tds.add("key1","data_new");
        logger.info("key1: " + tds.get("key1"));
        tds.replace("key1","data_updated");
        logger.info("key1: " + tds.get("key1"));
        logger.info("**************************************************************");

    }
}
