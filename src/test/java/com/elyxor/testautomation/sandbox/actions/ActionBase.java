package com.elyxor.testautomation.sandbox.actions;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.utils.driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ActionBase {

    public WebDriverWait wait;

    protected ActionBase() throws Exception {

        WebDriver webDriver = Driver.webDriver;

        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        Integer webdriverwait = Integer.parseInt(configInstance.getValue("webdriver.webdriverwait"));
        this.wait = new WebDriverWait(webDriver, webdriverwait);
    }

    public boolean waitForObject(By by) {
        try
        {
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
