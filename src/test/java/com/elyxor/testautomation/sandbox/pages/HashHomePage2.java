package com.elyxor.testautomation.sandbox.pages;


import com.elyxor.testautomation.sandbox.controls.AnotherButton;
import com.elyxor.testautomation.sandbox.controls.TextBox;
import com.elyxor.testautomation.pages.hash.PageBase;
import com.paulhammant.ngwebdriver.ByAngular;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class HashHomePage2 extends PageBase {

    public AnotherButton loginButton() {
        //return new MyCustomElement(webDriver, By.xpath("/html/body/reference/app-root/app-sign-in/div/div/div/div/div[2]/button"));
        return new AnotherButton(this.getWebDriver(), ByAngular.buttonText("Log In"));
    }

    public TextBox userTextBox() {
        return new TextBox(this.getWebDriver(), By.cssSelector("body > reference > app-root > app-sign-in > div > div > div > div > div.card-block > div:nth-child(1) > input"));
        //return new TextBox(webDriver, ByAngular.cssContainingText("form-control ng-untouched ng-pristine ng-valid","User Name"));
    }

    public TextBox passTextBox() {
        return new TextBox(this.getWebDriver(), By.cssSelector("body > reference > app-root > app-sign-in > div > div > div > div > div.card-block > div:nth-child(2) > input"));
        //return new TextBox(webDriver, ByAngular.cssContainingText("form-control ng-untouched ng-pristine ng-valid","Password"));
    }

    public HashHomePage2(WebDriver webDriver) {
        super(webDriver);
        //PageFactory.initElements(webDriver, this);
    }
}
