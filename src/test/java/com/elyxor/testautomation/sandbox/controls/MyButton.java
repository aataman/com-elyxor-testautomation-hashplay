package com.elyxor.testautomation.sandbox.controls;

import org.openqa.selenium.WebElement;

public class MyButton extends ElementImpl {

    public MyButton(WebElement element) {
        super(element);
    }

    public void myClick() {
        this.click();
    }
}
