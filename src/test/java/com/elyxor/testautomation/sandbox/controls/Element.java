package com.elyxor.testautomation.sandbox.controls;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.HasIdentity;
import org.openqa.selenium.internal.WrapsElement;


public interface Element extends WebElement, WrapsElement, HasIdentity {}

