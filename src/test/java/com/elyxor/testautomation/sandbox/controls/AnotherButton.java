package com.elyxor.testautomation.sandbox.controls;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AnotherButton extends UIElement {

    public AnotherButton(WebDriver driver, By signInButtonLocator) {
        super(driver, signInButtonLocator);
    }

    public void click() {
        element().click();
    }
}
