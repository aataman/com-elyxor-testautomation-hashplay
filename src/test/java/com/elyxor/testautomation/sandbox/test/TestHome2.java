package com.elyxor.testautomation.sandbox.test;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.sandbox.pages.HashHomePage2;
import com.elyxor.testautomation.test.hash.TestBase;
import org.openqa.selenium.JavascriptExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;


import com.paulhammant.ngwebdriver.NgWebDriver;

public class TestHome2 extends TestBase {

    private static final Logger logger = LoggerFactory.getLogger(TestHome2.class);

    HashHomePage2 objHomePage;
    private NgWebDriver ngWebDriver;


    @Test
    public void test() {

        ngWebDriver = new NgWebDriver((JavascriptExecutor) webDriver);

        webDriver.get(baseurl);
        ngWebDriver.waitForAngularRequestsToFinish();

        //objHomePage = PageFactory.initElements(webDriver, HashHomePage2.class);
        objHomePage = new HashHomePage2(webDriver);

        // TODO read from input service once ready not config service
        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        String user = String.format("%s", configInstance.getValue("app.hash.user"));
        String password = String.format("%s", configInstance.getValue("app.hash.pass"));

        //this.pauseTest(2000);
        //webDriver.findElement(By.cssSelector("#form-group")).sendKeys(user);
        //webDriver.findElement(ByAngular.cssContainingText("form-group","Password")).sendKeys(password);
        //this.pauseTest(2000);
        //webDriver.findElement(ByAngular.buttonText("Log in")).click();

        objHomePage.userTextBox().fill(user);
        objHomePage.passTextBox().fill(password);

        objHomePage.screenshot("HomePage");
        objHomePage.loginButton().click();

        //this.pauseTest(5000);
        //objHomePage.screenshot("FlightsPage");

    }
}
