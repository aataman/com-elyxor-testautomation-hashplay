package com.elyxor.testautomation.utils.testdata;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

// inspired from: https://dzone.com/articles/processing-json-with-jackson

/*
Usage Example:
String theFile = "testcases.json"
TestData td = new TestData(theFile);
td.printTestCaseData("TestLoginBadUserID");
logger.info("Get user: " + td.get("TestLoginBadUserID", "user"));
*/

public class TestData {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private JsonNode rootNode;
    private String testCaseFile;


    ObjectMapper objectMapper;

    public void load(String testCaseFile) throws IOException {
        objectMapper = new ObjectMapper();
        rootNode = objectMapper.readTree(new FileInputStream(testCaseFile));
        this.testCaseFile = testCaseFile;
    }

    public String get(String testCase, String key)
    {
        JsonNode testCaseNode = rootNode.path(testCase);
        Map<String, String> testCaseMap = objectMapper.convertValue(testCaseNode, Map.class);
        return testCaseMap.get(key);
    }

    private Map<String, String> getTestCaseMap(String testCase)
    {
        JsonNode testCaseNode = rootNode.path(testCase);
        Map<String, String> testCaseMap = objectMapper.convertValue(testCaseNode, Map.class);
        return testCaseMap;
    }

    public void printTestCaseData(String testCase)
    {
        Map<String, String> testCaseMap = getTestCaseMap(testCase);
        logger.info("---------------------------------------------");
        logger.info("Using file: " + this.testCaseFile);
        if (testCaseMap != null) {
            logger.info("Test Data for '" + testCase + "'");
            for (Map.Entry<String, String> entry : testCaseMap.entrySet()) {
                logger.info(entry.getKey() + " = " + entry.getValue());
            }
        }
        else {
            logger.info("Test Case '" + testCase + "' Not Found");
        }
        logger.info("---------------------------------------------");
    }
}

