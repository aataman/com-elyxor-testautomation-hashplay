package com.elyxor.testautomation.utils.testdata;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// inspired from: https://dzone.com/articles/processing-json-with-jackson

public class TestDataSimple {

    protected Map<String, String> testDataMap;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public void load(String theFile) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        testDataMap = objectMapper.readValue(new FileInputStream(theFile), Map.class);
    }

    public void add(String key, String value) {
        testDataMap.put(key,value);
    }

    public String get(String key) {
        return testDataMap.get(key);
    }

    public void remove(String key) {
        testDataMap.remove(key);
    }

    public void clear() {
        testDataMap.clear();
    }

    public void replace(String key, String value) {
        testDataMap.replace(key, value);
    }

    public void printMap() {
        logger.info("TestDataSimple ----------------------------" );
        for (Map.Entry<String, String> entry : testDataMap.entrySet()) {
            logger.info(entry.getKey() + "=" + entry.getValue());
        }
        logger.info("TestDataSimple ----------------------------" );
    }
}

