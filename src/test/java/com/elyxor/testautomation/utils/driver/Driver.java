package com.elyxor.testautomation.utils.driver;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Driver {

    // Holds the WebDriver instance
    public static WebDriver webDriver;

    public String browser;

    private static final Logger logger = LoggerFactory.getLogger(Driver.class);

    // Initialize a webDriver instance of required browser
    // Since this does not have a significance in the application's business domain, the BeforeSuite hook is used to instantiate the webDriver
    public void initializeDriver(String browser) {

        this.browser = browser;
        logger.info("initializeDriver: Browser: " + browser);
        webDriver = DriverFactory.getWebDriver(browser);

    }

    // Close the webDriver instance
    public void closeDriver(){
        if (webDriver != null) {
            logger.info("closeDriver: Closing Browser");
            webDriver.close();
            webDriver.quit();
        }
        else {
            logger.info("closeDriver: No Browser to close.");
        }
    }

}
