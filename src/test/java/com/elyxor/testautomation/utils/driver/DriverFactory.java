package com.elyxor.testautomation.utils.driver;

import java.util.concurrent.TimeUnit;

import com.elyxor.testautomation.configuration.ConfigurationSource;
import com.elyxor.testautomation.test.hash.TestLoginBadUserID;
import org.apache.commons.lang3.exception.ContextedRuntimeException;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DriverFactory {

    private static final Logger logger = LoggerFactory.getLogger(TestLoginBadUserID.class);

    private enum OS {
        WINDOWS, LINUX, MAC, SOLARIS
    }

    private static OS getOS() {
        OS os = null;
        String operSys = System.getProperty("os.name").toLowerCase();
        if (operSys.contains("win")) {
            os = OS.WINDOWS;
        } else if (operSys.contains("nix") || operSys.contains("nux")
                || operSys.contains("aix")) {
            os = OS.LINUX;
        } else if (operSys.contains("mac")) {
            os = OS.MAC;
        } else if (operSys.contains("sunos")) {
            os = OS.SOLARIS;
        }
        return os;
    }

    public static WebDriver getWebDriver(String browserType) {

        WebDriver webDriver;

        ConfigurationSource configInstance = TestAutomationConfiguration.getInstance();

        ChromeOptions chromeOptions = new ChromeOptions();
        String webdriverchromedriver;
        String browserchromelocation = null;

        switch (browserType) {

            case "chrome":

                // determine OS so that use the right Chrome Driver
                // https://sites.google.com/a/chromium.org/chromedriver/downloads
                switch (getOS()) {
                    case WINDOWS:
                        webdriverchromedriver = String.format("%s", configInstance.getValue("webdriver.chrome.driver.win32"));
                        break;
                    case MAC:
                        webdriverchromedriver = String.format("%s", configInstance.getValue("webdriver.chrome.driver.mac64"));
                        break;
                    case LINUX:
                        webdriverchromedriver = String.format("%s", configInstance.getValue("webdriver.chrome.driver.linux64"));
                        break;
                    default:
                        throw new ContextedRuntimeException(String.format("OS not supported"));
                }
                System.setProperty("webdriver.chrome.driver", webdriverchromedriver);
                logger.info("webdriverchromedriver: " + webdriverchromedriver);

                chromeOptions.addArguments("--no-sandbox");

                // get optional overrides
                // headless flag
                String headless = String.format( "%s",configInstance.getValue("browser.chrome.headless") );
                logger.info("headless: " + headless);

                if (headless.toLowerCase().equals("true")) {
                    chromeOptions.setHeadless(Boolean.TRUE);
                }

                browserchromelocation = String.format( "%s", configInstance.getValue("browser.chrome.location") );
                logger.info("browserchromelocation: " + browserchromelocation);

                if (browserchromelocation == null) {
                    chromeOptions.setBinary(browserchromelocation);
                }

                // create the driver instance
                webDriver = new ChromeDriver(chromeOptions);

                // override the default driver behavior
                // full screen
                String fullscreen = String.format( "%s",configInstance.getValue("webdriver.window.fullscreen") );
                if (fullscreen.toLowerCase().equals("true")) {
                    webDriver.manage().window().fullscreen();
                }

                // implicit wait
                Integer implicitwait = Integer.parseInt(configInstance.getValue("webdriver.implicitwait"));
                if (implicitwait != null) {
                    webDriver.manage().timeouts().implicitlyWait(implicitwait, TimeUnit.SECONDS);
                }

                return webDriver;

            default:
                throw new ContextedRuntimeException(String.format("Browser type unsupported: %s", browserType));

        }
    }
}
