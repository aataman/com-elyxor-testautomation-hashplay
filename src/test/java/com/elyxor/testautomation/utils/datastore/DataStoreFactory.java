package com.elyxor.testautomation.utils.datastore;

import com.elyxor.testautomation.utils.datastore.model.TestDataStore;
import org.apache.commons.lang3.exception.ContextedRuntimeException;

public class DataStoreFactory {

    public static TestDataStore getDataStore(String dataStoreType) {

        switch (dataStoreType)  {

            case "testdata":
                return new TestDataStore();

            default:
                throw new ContextedRuntimeException(String.format("Invalid dataStoreType: %s", dataStoreType));

        }

    }
}
