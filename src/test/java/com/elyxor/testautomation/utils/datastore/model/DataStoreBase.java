package com.elyxor.testautomation.utils.datastore.model;

public abstract class DataStoreBase {

    abstract void add(String key, Object value);

    abstract void replace(String key, Object value);

    abstract void remove(String key);

    abstract Object get(String key);

    abstract void clear();

}
