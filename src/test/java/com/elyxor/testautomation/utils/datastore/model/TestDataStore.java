package com.elyxor.testautomation.utils.datastore.model;

import java.util.HashMap;
import java.util.Map;

public class TestDataStore extends DataStoreBase {

    protected static Map testDataStore = new HashMap();

    @Override
    public void add(String key, Object value) {
        testDataStore.put(key,value);
    }

    @Override
    public Object get(String key) {
        return testDataStore.get(key);
    }

    @Override
    public void remove(String key) {
        testDataStore.remove(key);
    }

    @Override
    public void clear() {
        testDataStore.clear();
    }

    @Override
    public void replace(String key, Object value) {
        testDataStore.replace(key, value);
    }
}
