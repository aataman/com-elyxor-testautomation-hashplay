package com.elyxor.testautomation.pages.element;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MyCustomElement extends WebElementBase {

    public MyCustomElement(WebDriver driver, By locator) {
        super(driver, locator);
    }

    public void click() {
        //wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        this.element().click();
    }
}