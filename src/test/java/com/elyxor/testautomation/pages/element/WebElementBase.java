package com.elyxor.testautomation.pages.element;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class WebElementBase {

    private WebDriver driver;
    private String id;

    protected By locator;
    protected WebDriverWait wait;

    public WebElementBase(WebDriver driver, By locator) {
        this.driver = driver;
        this.locator = locator;
    }

    protected WebElement element(){
        //wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        //wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        return driver.findElement(locator);
    }

    // these are ideas if feel need them in base
    public void assertContainsText(String txt) {
        assertThat(element().getText(),containsString(txt));
    }

    public void assertHasCSSClass(String cssClass) {
        String actualCSSClasses = element().getAttribute("class");
        assertThat(actualCSSClasses, containsString(cssClass));
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getID() {
        return this.id;
    }
}
