package com.elyxor.testautomation.pages.hash;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

public abstract class PageBase {

    public WebDriverWait wait;

    private WebDriver webDriver;

    protected PageBase(WebDriver webDriver) {
        this.webDriver = webDriver;

        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        Integer webdriverwait = Integer.parseInt(configInstance.getValue("webdriver.webdriverwait"));
        this.wait = new WebDriverWait(webDriver, webdriverwait);
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public String getTitle() {
        return webDriver.getTitle();
    }

    public String screenshot(String filename) {

        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        String user = String.format("%s", configInstance.getValue("app.hash.user"));

        File source = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
        String path = String.format("./build/reports/screenshots/%s.png", filename);
        try {
            FileUtils.copyFile(source, new File(path));
        }
        catch(IOException e) {
            path = "Failed to capture screenshot: " + e.getMessage();
        }
        return path;
    }

    public boolean waitForObject(By by) {
        try
        {
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

}
