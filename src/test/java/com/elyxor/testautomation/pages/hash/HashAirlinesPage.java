package com.elyxor.testautomation.pages.hash;

import java.util.List;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;



/**
 * Created by aataman on 3/15/18.
 */
public class HashAirlinesPage extends PageBase{

    private static final String TXT_USERNAME_XPATH = "//input[@placeholder=\"User Name\"]";
    private static final String TXT_PASSWORD_XPATH = "//input[@placeholder=\"Password\"]";
    private static final String BNT_LOGIN_XPATH = "/html/body/reference/app-root/app-sign-in/div/div/div/div/div[2]/button";
    private static final String ROW_TABLEROW_XPATH = "//*/datatable-row-wrapper[.]/datatable-body-row";
    private static final String CEL_TABLECELL_XPATH = ".//div[.]/datatable-body-cell[.]";
    private static final String BTN_EDIT_XPATH = ".//span[@class=\"fa fa-pencil\"]";
    private static final String BTN_DELETE_XPATH = ".//span[@class=\"fa fa-trash\"]";
    private static final String BTN_CLOSE_XPATH = "//button[@class=\"close\"]";
    private static final String BTN_CANCEL_XPATH = "//button[@data-dismiss=\"modal\"]";
    private static final String ADD_NEW_BTN = "/html/body/reference/app-root/app-dashboard/app-airlines/div/div/div/span/button/span";
    private static final String TXT_NAME_XPATH = "//input[@placeholder='Name']";
    private static final String BTN_CREATE = "//*[@id='update-title']";
    private static final String BTN_REMOVE = "//*[@id='btn-remove']";
    private static final String BTN_UPDATE= "//*[@id='btn-update']";


    @FindBy(xpath = ROW_TABLEROW_XPATH)
    @CacheLookup
    WebElement tableRow;

    @FindBy(xpath = CEL_TABLECELL_XPATH)
    @CacheLookup
    WebElement tableCell;

    @FindBy(xpath = ADD_NEW_BTN)
    @CacheLookup
    WebElement addButton;

    @FindBy(xpath = TXT_NAME_XPATH)
    @CacheLookup
    WebElement txtName;

    @FindBy(xpath = BTN_CREATE)
    @CacheLookup
    WebElement createButton;

    @FindBy(xpath = BTN_REMOVE)
    @CacheLookup
    WebElement removeButton;

    @FindBy(xpath = BTN_UPDATE)
    @CacheLookup
    WebElement updateButton;


    public HashAirlinesPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(ROW_TABLEROW_XPATH)));
    }



    public  String GetNumberOfAirlines(){
        List<WebElement> tableRows = this.getWebDriver().findElements(By.xpath(ROW_TABLEROW_XPATH));
        return String.valueOf(tableRows.size());
    }

    public void AddNewItem(String name){
        addButton.click();
        sleep(2);
        txtName.sendKeys(name);
        sleep(2);
        createButton.click();
    }

    public void RemoveItem(String name){
        clickDeleteButton(name);
        sleep(2);
        removeButton.click();
    }


    public void RemoveItem(){
        List<WebElement> tableRows = this.getWebDriver().findElements(By.xpath(ROW_TABLEROW_XPATH));

        for (WebElement row : tableRows) {
            List<WebElement> cells = this.getWebDriver().findElements(By.xpath(CEL_TABLECELL_XPATH));
            System.out.println(cells.size());

            cells.get(9).click();
            for (WebElement cell : cells) {
                cell.getTagName();

            }
            System.out.println("++++++++End Row++++++++");
        }
    }

    public void closeEditModal(){
        this.getWebDriver().findElement(By.xpath(BTN_CLOSE_XPATH)).click();
    }

    public void clickCancel(){
        this.getWebDriver().findElement(By.xpath(BTN_CANCEL_XPATH)).click();
    }

    public void clickUpdateButton(){
        this.getWebDriver().findElement(By.xpath(BTN_UPDATE)).click();
    }

    public void clickEditButton(int rowNumber) {
        List<WebElement> tableRows =  this.getWebDriver().findElements(By.xpath(ROW_TABLEROW_XPATH));
        tableRows.get(rowNumber - 1).findElement(By.xpath(BTN_EDIT_XPATH)).click();
    }

    public void clickEditButton(String airlineName){
        List<WebElement> tableRows =  this.getWebDriver().findElements(By.xpath(ROW_TABLEROW_XPATH));
        for(WebElement row : tableRows){
            List<WebElement> cells = row.findElements(By.xpath(CEL_TABLECELL_XPATH));
            if(airlineName.equals(cells.get(0).getText())){
                row.findElement(By.xpath(BTN_EDIT_XPATH)).click();
                break;
            }
        }
    }

    public Boolean checkAirlineExists(String airlineName) {
        List<WebElement> tableRows = this.getWebDriver().findElements(By.xpath(ROW_TABLEROW_XPATH));
        for (WebElement row : tableRows) {
            List<WebElement> cells = row.findElements(By.xpath(CEL_TABLECELL_XPATH));
            if (airlineName.equals(cells.get(0).getText())) {
                return true;
            }
        }
        return false;
    }


        public void clickDeleteButton(int rowNumber) {
        List<WebElement> tableRows =  this.getWebDriver().findElements(By.xpath(ROW_TABLEROW_XPATH));
        tableRows.get(rowNumber - 1).findElement(By.xpath(BTN_DELETE_XPATH)).click();
    }

    public void clickDeleteButton(String airlineName){
        List<WebElement> tableRows =  this.getWebDriver().findElements(By.xpath(ROW_TABLEROW_XPATH));
        for(WebElement row : tableRows){
            List<WebElement> cells = row.findElements(By.xpath(CEL_TABLECELL_XPATH));
            if(airlineName.equals(cells.get(0).getText())){
                row.findElement(By.xpath(BTN_DELETE_XPATH)).click();
                break;
            }
        }
    }
    public void confirmDelete(){
        removeButton.click();
    }

    public void updateAirlineName(String name){
        txtName.sendKeys(name);
    }

    public void printTableCells() {
        List<WebElement> tableRows = this.getWebDriver().findElements(By.xpath(ROW_TABLEROW_XPATH));
        System.out.println("Number of rows in table: " + String.valueOf(tableRows.size()));

        for (WebElement row : tableRows) {
            System.out.println("++++++++Start Row++++++++");
            List<WebElement> cells = row.findElements(By.xpath(CEL_TABLECELL_XPATH));
            System.out.println("Number of cells in row: " + cells.size());
            for (WebElement cell : cells) {
                System.out.println(cell.getText());
            }
            System.out.println("++++++++End Row++++++++");
        }
    }

    public void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
