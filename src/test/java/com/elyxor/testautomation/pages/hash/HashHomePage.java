package com.elyxor.testautomation.pages.hash;

import com.elyxor.testautomation.pages.element.MyCustomElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;

public class HashHomePage extends PageBase {

    private static final String TXT_USERNAME_XPATH = "//input[@placeholder='User Name']";
    private static final String TXT_PASSWORD_XPATH = "//input[@placeholder='Password']";
    private static final String BTN_LOGIN_XPATH = "/html/body/reference/app-root/app-sign-in/div/div/div/div/div[2]/button";
    private static final String LBL_ERRORTEXT_CSS = "body > reference > app-root > app-sign-in > div > div > div > div > div.card-block > div.alert.alert-danger > span";


    @FindBy(xpath = TXT_USERNAME_XPATH )
    @CacheLookup
    WebElement textBoxUser;

    @FindBy(xpath = TXT_PASSWORD_XPATH)
    @CacheLookup
    WebElement textBoxPassword;

    @FindBy(xpath = BTN_LOGIN_XPATH)
    @CacheLookup
    WebElement buttonLogin;

    public MyCustomElement customElement;

    public HashHomePage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(BTN_LOGIN_XPATH)));

        customElement = new MyCustomElement(webDriver, By.xpath(BTN_LOGIN_XPATH));
    }

    public void setUser(String user) {
        textBoxUser.sendKeys(user);
    }

    public void setPassword(String password) {
        textBoxPassword.sendKeys(password);
    }

    public void clickButtonLogin() {
        buttonLogin.click();
    }

    public void login(String user, String password) {
        setUser(user);
        setPassword(password);
        clickButtonLogin();
    }

    public String getErrorText() {
        try {
            this.waitForObject(By.cssSelector(LBL_ERRORTEXT_CSS));
            WebElement errorText = this.getWebDriver().findElement(By.cssSelector(LBL_ERRORTEXT_CSS));
            return errorText.getText();
        }
        catch (Exception e) {
            // throw org.openqa.selenium.TimeoutException
            return "";
        }

    }
}
