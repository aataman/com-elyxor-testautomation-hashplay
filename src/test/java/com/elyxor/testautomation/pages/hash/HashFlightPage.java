package com.elyxor.testautomation.pages.hash;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HashFlightPage extends PageBase {

    private static final String MNU_MAIN_XPATH = "//*[@id=\"navbarDropdownMenuLink\"]";
    private static final String MNU_MAIN_LOGOUT_CSS = "body > reference > app-root > app-dashboard > app-navbar > nav > ul:nth-child(2) > li > div > a > i";
    private static final String LBL_TOTAL_AIRLINES_CSS = "/html/body/reference/app-root/app-dashboard/app-navbar/nav/ul[2]/li/div/a";

    @FindBy(xpath = MNU_MAIN_XPATH)
    @CacheLookup
    WebElement menu;

    @FindBy(xpath = MNU_MAIN_LOGOUT_CSS)
    @CacheLookup
    WebElement menuOptionLogout;

    @FindBy(xpath = LBL_TOTAL_AIRLINES_CSS)
    @CacheLookup
    WebElement textTotalAirlines;

    public HashFlightPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(LBL_TOTAL_AIRLINES_CSS)));
    }

    public void clickMenu() {
        menu.click();
    }

    public void clickMenuOptionLogout() {
        menuOptionLogout.click();
    }

    public void getTextTotalAirlines() {
        textTotalAirlines.getText();
    }

}
